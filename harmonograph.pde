import netP5.*;
import oscP5.*;
OscP5 oscP5;
int counter = 0;
int f = 0;
NetAddress puredata;
float t = 0;
ArrayList<PVector> points = new ArrayList<PVector>();
PVector tmp;
int j;
int range = 250;
//String savePlace = "data/animations/"+day()+"-"+month()+"-"+year()+"/anim-"+hour()+""+minute()+""+second()+"/";
void setup(){
  size(10000, 10000);
  frameRate(1000);
  stroke(0,0,0,10);
  //strokeWeight(10);
  background(255);
  //oscP5 = new OscP5(this, 12000);
  //puredata = new NetAddress("127.0.0.1",8000);
  //point(width/2,height/2);
}

void draw(){
 points.add(new PVector(width/2+(4700*sin(t*220)*pow((float)Math.E, (float) 0.01*t))
                               //+(500*sin(t*15.36)*pow((float)Math.E, (float) -0.01*t))
                      ,height/2//+(500*sin(t*32.71)*pow((float)Math.E, (float) -0.01*t))
                               +(4700*sin(t*277.183)*pow((float)Math.E, (float) 0.01*t))));
 tmp = points.get(points.size()-1);
 point(tmp.x, tmp.y);
 //oscP5.send(new OscMessage("/h").add(abs(tmp.x-width/2)).add(abs(tmp.y-height/2)), puredata);
 for(j=0; j<points.size(); j++){
  PVector a, b;
  if(distance(a=tmp, b=points.get(j)) < range)
   line(a.x, a.y, b.x, b.y);
 }
  t+=.00005;
  //counter++;
  //if(counter%42==0){
  //  saveFrame(savePlace+f+".png");
  //  f++;
  //}
}

void keyPressed(){
  switch(key){
    case 'q': saveFrame(day()+""+month()+""+year()+""+hour()+""+minute()+""+second()+".png");println("done");break;
    default:break;
  }
  
}

float distance(PVector a, PVector b){
  float result;
  float xpart, ypart;
  xpart = (a.x - b.x) * (a.x - b.x); 
  ypart = (a.y - b.y) * (a.y - b.y);
  result = sqrt(xpart + ypart);
  return result;
}