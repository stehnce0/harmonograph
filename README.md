# harmonograph

This program lets the user draw out harmonies using additive frequencies. Modifying the output size and various numbers within code produces very visually pleasing results.
There is no interface, you must edit the code directly.
Some examples are here: 

https://drive.google.com/file/d/1yjfjsbWx-kShGc5F9SLbHPXGBD7WYPI0/view?usp=sharing

https://drive.google.com/file/d/1msGCnCke_xSHtLBJo-loAVHA6CwI2Lup/view?usp=sharing